using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class UnshureAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Condition { get; protected set; }

		public UnshureAttribute([NotNull] string condition) {
			Condition = condition;
		}
	}
}