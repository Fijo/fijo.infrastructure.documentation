using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	public enum ObjectType {
		Class,
		Function,
		Functionality,
		Structure,
		Struct,
		Interface,
		Enum,
		EnumValue
	}
}