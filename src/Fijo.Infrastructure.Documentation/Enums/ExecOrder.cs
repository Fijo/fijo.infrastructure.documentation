using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	public enum ExecOrder {
		[PublicAPI] After,
		[PublicAPI] Before
	}
}