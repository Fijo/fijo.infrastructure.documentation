using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class OnlyPublicForTestingAttribute : OnlyPublicForAttribute {
		public OnlyPublicForTestingAttribute() : base("Testing") {}

		public OnlyPublicForTestingAttribute([NotNull] string message) : base(string.Concat("Testing, ", message)) {}
	}
}