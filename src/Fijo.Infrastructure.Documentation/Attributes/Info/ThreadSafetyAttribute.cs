using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
	[Desc("Descripes a the the thread safety of the target it tis applied to")]
	public class ThreadSafetyAttribute : Attribute {
		[PublicAPI]
		public ThreadSafety ThreadSafety { get; protected set; }
		[CanBeNull, PublicAPI]
		public string Note { get; set; }

		public ThreadSafetyAttribute(ThreadSafety threadSafety) {
			ThreadSafety = threadSafety;
		}

		public ThreadSafetyAttribute(ThreadSafety threadSafety, [NotNull] string note) : this(threadSafety) {
			Note = note;
		}
	}
}