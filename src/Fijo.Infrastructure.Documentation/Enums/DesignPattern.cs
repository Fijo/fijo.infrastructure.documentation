using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	[ModificationNote("Attributes may be copired (in some parts or fully) to the DesignPatternAttribute classes that uses the EnumValue the attribute is attratched to.")]
	public enum DesignPattern {
		AbstractFactory,
		Builder,
		[AboutName("D", "Data"), AboutName("A", "Access"), AboutName("O", "Object")] Dao,
		[AboutName("D", "Data"), AboutName("T", "Transfer"), AboutName("O", "Object")] Dto,
		Entity,
		Provider,
		Repository,
		Store,
		Stock,
		FactoryMethod,
		Service,
		Prototype,
		Singleton,
		Adapter,
		Bridge,
		Composite,
		Controller,
		Decorator,
		Facade,
		Flyweight,
		Proxy,
		ChainOfResponsibility,
		Command,
		Interpreter,
		Iterator,
		Mediator,
		Memento,
		[Note("used to watch ObserverSubjects")]
		[Related(ObjectType.EnumValue, "Fijo.Infrastructure.Documentation.Enums.DesignPattern.ObserverManager", "used to watches that object (can be registered to an ´ObserverManager´).")]
		Observer,
		[Note("Something that can be seen by an Observer")]
		[Related(ObjectType.EnumValue, "Fijo.Infrastructure.Documentation.Enums.DesignPattern.Observer", "a part of the Observer pattern")]
		[Note("more or less a Design Pattern (a part of the Observer pattern)")]
		[Desc("An object that is ´silly´ an that is used as state to be given to the ´ObserverManager´ who passes it to the to him (´ObserverManager´) registered ´Observer´(s)")]
		[About("ObserverableSubject")]
		Observerable,
		[Note("An object that can be watched by an observer")]
		[RelatedLink("http://www.dofactory.com/Patterns/PatternObserver.aspx", LinkNote= "The ´Subject´ is used as this object there, but not the ´ConcreteSubject´. In difference to the website my Subject ist independent from the ´ConcreteSubject´ what is the Observerable for me. It does not inherit from the ´Subject´/ this object. So in some way this is something between the ´Subject´ and the ´Observer´ on the website.")]
		[Related(ObjectType.EnumValue, "Fijo.Infrastructure.Documentation.Enums.DesignPattern.Observer", "a part of the Observer pattern")]
		[Note("more or less a Design Pattern (a part of the Observer pattern)")]
		ObserverManager,
		State,
		Strategy,
		TemplateMethod,
		Visitor
	}
}