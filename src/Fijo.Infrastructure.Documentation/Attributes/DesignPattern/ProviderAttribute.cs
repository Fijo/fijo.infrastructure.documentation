using System;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[Related(ObjectType.Class, "Fijo.Infrastructure.Documentation.Attributes.Repository", "Can contain logic")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	[UsageInherited(typeof(CannotApplyEqualityOperatorAttribute))]
	public class ProviderAttribute : DesignPatternAttribute {
		public ProviderAttribute() : base(Enums.DesignPattern.Provider) {}
	}
}