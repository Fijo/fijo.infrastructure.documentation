using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternChain.aspx")]
	public class ChainOfResponsibilityAttribute : DesignPatternAttribute {
		public ChainOfResponsibilityAttribute() : base(Enums.DesignPattern.ChainOfResponsibility) {}
	}
}