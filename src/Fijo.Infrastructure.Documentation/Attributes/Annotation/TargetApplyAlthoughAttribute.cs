using System;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	[BaseTypeRequired(typeof(Attribute))]
	[Desc("Says that the target (an attribute) should although apply another attribute to its target.")]
	[Related(ObjectType.Class, "JetBrains.Annotations.BaseTypeRequiredAttribute", "same thing but with Attributes that are although applied to the target of the attribute that has this attribute instead of base type (inherited types/ interfaces) of the target of an attribute that has this attribute. Please note the second difference that the attributes do not have to be applied at the target of the target attribute - they will automaticle be applied (currently not but I want to write something that do add them)")]
	[Note("currently this is only used for design structure and documentation - the addidional Attributes will not be realy added to the targets/ usages (where the attribute is applied) of the target attribute")]
	public class TargetApplyAlthoughAttribute : Attribute {
		private readonly Attribute _attribute;
		public Attribute Attribute { get { return _attribute; } }

		public TargetApplyAlthoughAttribute([NotNull] Type attribute, [NotNull] params object[] args) {
			Debug.Assert(attribute.IsSubclassOf(typeof (Attribute)), "attribute has to inherit from Attribute");
			var constructor = attribute.GetConstructor(args.Select(x => x.GetType()).ToArray());
			Debug.Assert(constructor != null);
			_attribute = (Attribute) constructor.Invoke(args);
		}
	}
}