using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	[Flags]
	public enum Platform {
		Dos = 1,
		Unix = 2,
		Windows = Dos | 4,
		Linux = Unix | 8,
		Mac = Unix | 16,
	}
}