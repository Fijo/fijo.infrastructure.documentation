using System;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	[UsageInherited(typeof(CannotApplyEqualityOperatorAttribute))]
	public class StockAttribute : DesignPatternAttribute {
		public StockAttribute() : base(Enums.DesignPattern.Stock) {}
	}
}