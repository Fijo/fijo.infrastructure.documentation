using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using Fijo.Infrastructure.Documentation.Provider;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	[Desc("Notes, that a class, methode or a feature in a class or a methode works only on spec versions")]
	public class OSRequiredAttribute : Attribute {
		public Platform Platform { get; private set; }
		public IList<WindowsVersion> WindowsVersions { get; private set; }
		public bool UseFeature { get; private set; }
		public string Feature { get; private set; }

		public OSRequiredAttribute(Platform platform, bool atLeast, WindowsVersion windowsVersion) {
			PreCondition(windowsVersion);
			Platform = platform;
			UseFeature = false;
			var version = (short) windowsVersion;
			WindowsVersions = GetWindowsVersions(atLeast, windowsVersion, version);
		}

		public OSRequiredAttribute(Platform platform, params WindowsVersion[] windowsVersions) {
			PreCondition(windowsVersions);
			Platform = platform;
			WindowsVersions = windowsVersions;
			UseFeature = false;
		}
		
		public OSRequiredAttribute(string feature, Platform platform, bool atLeast, WindowsVersion windowsVersion) : this(platform, atLeast, windowsVersion) {
			UseFeature = true;
			Feature = feature;
		}

		public OSRequiredAttribute(string feature, Platform platform, params WindowsVersion[] windowsVersions) : this(platform, windowsVersions) {
			UseFeature = true;
			Feature = feature;
		}

		private WindowsVersion[] GetWindowsVersions(bool atLeast, WindowsVersion windowsVersion, short version) {
			return atLeast
				       ? OSVersionProvider.WindowsVersions.Where(x => (short) x >= version).ToArray()
				       : new[] {windowsVersion};
		}

		private void PreCondition(WindowsVersion windowsVersion) {
			if (OSVersionProvider.IllegalWindowsVersions.Contains(windowsVersion)) throw new ArgumentException("Invalid WindowsVersion enum value", "windowsVersion");
		}

		private void PreCondition(IEnumerable<WindowsVersion> windowsVersions) {
			if (windowsVersions.Any(x => OSVersionProvider.IllegalWindowsVersions.Contains(x))) throw new ArgumentException("Invalid WindowsVersion enum value", "windowsVersions");
		}
	}
}