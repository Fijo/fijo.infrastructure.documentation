using System;
using System.ComponentModel;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[TargetApplyAlthough(typeof (EditorBrowsableAttribute), EditorBrowsableState.Advanced)]
	[MeansImplicitUse]
	[Related(ObjectType.Class, "JetBrains.Annotations.PublicAPI", "same but for stuff, that is to extend stuff but still can be documented in a public ŽextentionŽ API.")]
	public class ExtentionAPIAttribute : Attribute {}
}