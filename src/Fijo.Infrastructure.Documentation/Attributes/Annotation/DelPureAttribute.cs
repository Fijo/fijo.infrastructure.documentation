using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Delegate, Inherited = true)]
	[AboutName("Del", "Delegate (pure for delegates)")]
	[Desc("Being pure indicates that method doesn't contain observable side effects. In this case it means that all implementations of the target delegate defenition it is applied to, are Pure.")]
	[Related(ObjectType.Class, "JetBrains.Annotations.PureAttribute", "same functionality")]
	public class DelPureAttribute : Attribute {}
}