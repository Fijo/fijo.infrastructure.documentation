using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class PerformanceRiskyBecauseHighAbstraction : PerformanceRisky {
		public PerformanceRiskyBecauseHighAbstraction() : base("HighAbstraction") {}
		public PerformanceRiskyBecauseHighAbstraction([NotNull] string why) : base(string.Concat("HighAbstraction, ", why)) {}
	}
}