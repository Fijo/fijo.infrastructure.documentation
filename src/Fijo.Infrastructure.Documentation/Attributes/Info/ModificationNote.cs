using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[Desc("Notes something you may have to know or to think of if changeing the target the attribute is applied to")]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class ModificationNote : Attribute {
		[NotNull, PublicAPI]
		public string Message { get; protected set; }

		public ModificationNote([NotNull] string message) {
			Message = message;
		}
	}
}