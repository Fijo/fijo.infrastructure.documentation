using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Delegate | AttributeTargets.Enum | AttributeTargets.Event | AttributeTargets.Field | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Property | AttributeTargets.Struct, Inherited = false, AllowMultiple = true)]
	[Desc("notes, that something is used for a spec. feature")]
	public class FeatureTypeAttribute : Attribute {
		public string Feature { get; set; }

		public FeatureTypeAttribute(string feature) {
			Feature = feature;
		}
	}
}