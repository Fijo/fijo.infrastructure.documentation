using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class OnlyPublicForAttribute : Attribute {
		[NotNull, PublicAPI]
		public string PublicUsage { get; protected set; }

		public OnlyPublicForAttribute([NotNull] string publicUsage) {
			PublicUsage = publicUsage;
		}
	}
}