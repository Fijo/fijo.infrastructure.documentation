using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	public enum IgnoreFor
	{
		EntityFramework
	}
}