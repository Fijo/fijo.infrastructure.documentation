using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Says that something has a pitfall, that often make developers, don�t want that function to do when using it. Such things are no bugs.")]
	public class PitfallAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Pitfall { get; protected set; }
		[CanBeNull, PublicAPI]
		public string Solution { get; protected set; }
		[CanBeNull, PublicAPI]
		public string Condition { get; protected set; }

		public PitfallAttribute([NotNull] string pitfall, [CanBeNull] string solution = default(string)) {
			Pitfall = pitfall;
			Solution = solution;
		}

		public PitfallAttribute([NotNull] string condition, [NotNull] string pitfall, [CanBeNull] string solution = default(string)) : this(pitfall, solution) {
			Condition = condition;
		}
	}
}