using System;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://msdn.microsoft.com/en-us/library/ff649690.aspx")]
	[Note("Contains no/ not mutch logic")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	[UsageInherited(typeof(CannotApplyEqualityOperatorAttribute))]
	public class RepositoryAttribute : DesignPatternAttribute {
		public RepositoryAttribute() : base(Enums.DesignPattern.Repository) {}
	}
}