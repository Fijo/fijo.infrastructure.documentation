using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.GenericParameter | AttributeTargets.Parameter | AttributeTargets.Property, Inherited = true)]
	[Desc("Informs about in which cases the IEnumerable will be executed. Should be applied on an IEnumerable parameter or field ...")]
	public class ExecutedAttribute : Attribute {
		[PublicAPI] public Cond Condition { get; private set; }
		[PublicAPI] public string AdvancedCondition { get; private set; }

		public ExecutedAttribute(Cond condition) {
			Condition = condition;
		}

		public ExecutedAttribute([NotNull] string someTimesCondtition) : this(Cond.Sometimes) {
			AdvancedCondition = someTimesCondtition;
		}
	}
}