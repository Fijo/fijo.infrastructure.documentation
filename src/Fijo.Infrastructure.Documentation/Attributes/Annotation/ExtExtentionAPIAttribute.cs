using System;
using System.ComponentModel;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[TargetApplyAlthough(typeof (EditorBrowsableAttribute), EditorBrowsableState.Never)]
	[MeansImplicitUse]
	[AboutName("Ext", "Extended")]
	[Related(ObjectType.Class, "Fijo.Infrastructure.Documentation.Attributes.Annotation.ExtentionAPIAttribute", "same but only for �hardcore� extention stuff that should not be viewed to normal developers - cause it is not needed in most cases only really only for real hardcore extentions that requires some more information, it is needed.")]
	public class ExtExtentionAPIAttribute : Attribute {}
}