using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Says, that using you have to think of some thing(s) - and you should be warned because of easyly usage without the result the user expects.")]
	public class DangerousAttribute : Attribute {
		[CanBeNull, PublicAPI]
		public string Condition { get; protected set; }
		[NotNull, PublicAPI]
		public string Why { get; protected set; }

		public DangerousAttribute([NotNull] string why)  {
			Why = why;
		}

		public DangerousAttribute([NotNull] string condition, [NotNull] string why) : this(why) {
			Condition = condition;
		}
	}
}