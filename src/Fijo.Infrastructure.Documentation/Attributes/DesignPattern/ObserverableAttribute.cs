﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[ModificationNote("Some Attributes are partly or fully copied from the ones of ´Enums.DesignPattern.Observerable´")]
	[Note("Something that can be seen by an Observer")]
	[Related(ObjectType.Class, "Fijo.Infrastructure.Documentation.Attributes.DesignPattern.ObserverAttribute", "a part of the Observer pattern")]
	[Note("more or less a Design Pattern (a part of the Observer pattern)")]
	[Desc("An object that is ´silly´ an that is used as state to be given to the ´ObserverManager´ who passes it to the to him (´ObserverManager´) registered ´Observer´(s)")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	[About("ObserverableSubjectAttribute")]
	public class ObserverableAttribute : DesignPatternAttribute {
		public ObserverableAttribute() : base(Enums.DesignPattern.Observerable) {}
	}
}