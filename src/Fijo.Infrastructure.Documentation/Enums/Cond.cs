using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	[AboutName("Cond", "Condition")]
	public enum Cond {
		Always,
		Sometimes,
		Never
	}
}