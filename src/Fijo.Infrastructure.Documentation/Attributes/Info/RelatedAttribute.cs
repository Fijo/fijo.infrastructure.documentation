using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("says that something is related to another thing.")]
	public class RelatedAttribute : Attribute {
		[CanBeNull, PublicAPI]
		public string ObjectType { get; protected set; }
		[PublicAPI]
		public ObjectType ObjectTypeEnum { get; protected set; }
		[NotNull, PublicAPI]
		public string RelatedThing { get; protected set; }
		[NotNull, PublicAPI]
		public string InWhichWay { get; protected set; }

		[Desc("objectType = what kind of thing is related, relatedThing = what thing is related, inWhichWay = how is it related")]
		public RelatedAttribute(ObjectType objectType, [NotNull] string relatedThing, [NotNull] string inWhichWay) {
			ObjectTypeEnum = objectType;
			RelatedThing = relatedThing;
			InWhichWay = inWhichWay;
		}

		[Desc("objectType = what kind of thing is related, relatedThing = what thing is related, inWhichWay = how is it related")]
		public RelatedAttribute([NotNull] string objectType, [NotNull] string relatedThing, [NotNull] string inWhichWay) {
			ObjectType = objectType;
			RelatedThing = relatedThing;
			InWhichWay = inWhichWay;
		}
	}
}