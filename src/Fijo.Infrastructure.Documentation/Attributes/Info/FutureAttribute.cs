﻿using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Inform about the fact, that something is created to be used for a planded future feature.")]
	public class FutureAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Feture { get; protected set; }

		public FutureAttribute([NotNull] string feture) {
			Feture = feture;
		}
	}
}