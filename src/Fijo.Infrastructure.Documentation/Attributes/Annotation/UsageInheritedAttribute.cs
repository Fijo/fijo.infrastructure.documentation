using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
	[BaseTypeRequired(typeof(Attribute))]
	[Desc("Specs, that the target Attribute sets should inherit some Attributes to its usage.")]
	public class UsageInheritedAttribute : Attribute {
		public IList<Type> AdditionalAttributes { get; protected set; }

		public UsageInheritedAttribute(params Type[] additionalAttributes) {
			#region PreCondition
			Debug.Assert(additionalAttributes != null, "additionalAttributes != null");
			Debug.Assert(additionalAttributes.All(typeof(Attribute).IsAssignableFrom), "additionalAttributes.All(typeof(Attribute).IsAssignableFrom)");
			#endregion
			AdditionalAttributes = additionalAttributes;
		}
	}
}