using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Descripes a somethings general function, very short, if its name doesn�t say enough")]
	public class AboutAttribute : Attribute {
		[NotNull, PublicAPI]
		public string About { get; protected set; }

		public AboutAttribute([NotNull] string about) {
			About = about;
		}
	}
}