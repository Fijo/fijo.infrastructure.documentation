using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Infrastructure.Documentation.Enums {
	[Flags]
	public enum ThreadSafety {
		ReqEnumeration = 2,
		[About("RequiresSaveParralelEnumerableExecutionSupport")]
		ReqParallelEnumeration = 4,
		Save = 0,
		Unsave = 1 | ReqEnumeration | ReqParallelEnumeration
	}
}