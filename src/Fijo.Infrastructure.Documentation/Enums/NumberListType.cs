using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	[Desc("The data type from what list of numbers can be created")]
	public enum NumberListType {
		MinMaxRange,
		Exclude,
		Include
	}
}