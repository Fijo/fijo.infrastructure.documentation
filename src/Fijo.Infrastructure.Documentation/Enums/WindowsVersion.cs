using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	[Flags, RelatedLink("http://de.wikipedia.org/wiki/Microsoft_Windows")]
	public enum WindowsVersion : short {
		#region BaseType
		DosBased = 512,
		NTBased = 1024,
		#endregion
		#region DosBased
		Win1 = DosBased | 1,
		Win2 = DosBased | 2,
		Win3 = DosBased | 3,
		Win95 = DosBased | 4,
		Win98 = DosBased | 5,
		WinMe = DosBased | 6,
		#endregion
		#region NTBased
		WinNT3_1 = NTBased | 1,
		WinNT3_5 = NTBased | 2,
		WinNT4_0 = NTBased | 3,
		Win2000 = NTBased | 4,
		WinXP = NTBased | 5,
		WinServer2003 = NTBased | 6,
		WinVista = NTBased | 7,
		WinServer2008 = NTBased | 8,
		Win7 = NTBased | 9,
		WinServer2008R2 = NTBased | 10,
		Win8 = NTBased | 11,
		WinServer2012 = NTBased | 12,
		#endregion
	}
}