using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[BaseTypeRequiredAttribute(typeof(IEnumerable))]
	[Desc("Says that the target this attribute is applied to (it has to be at lest a IEnumerable better a ICollection) can only have sizes as speced (so it an Assertation for the Count of elements in a IEnumerable)")]
	[AttributeUsage(AttributeTargets.ReturnValue | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Field | AttributeTargets.Module | AttributeTargets.Property | AttributeTargets.Struct | AttributeTargets.Parameter, Inherited = true, AllowMultiple = true)]
	[About("CollectionCountAsserationAttribute")]
	public class SizeAttribute : Attribute {
		[PublicAPI]
		public NumberListType NumberListType { get; protected set; }
		[PublicAPI]
		public int Min { get; protected set; }
		[PublicAPI]
		public int Max { get; protected set; }
		[CanBeNull, PublicAPI]
		public ISet<int> Exclude { get; protected set; }
		[CanBeNull, PublicAPI]
		public ISet<int> Include { get; protected set; }

		public SizeAttribute(int min, int max = int.MaxValue) {
			#region PreCondition
			if(max <= min) throw new ArgumentOutOfRangeException("max", max, "�max� has to be bigger or eqaul with the �min�");
			#endregion
			NumberListType = NumberListType.MinMaxRange;
			Min = min;
			Max = max;
		}
		
		public SizeAttribute(SizeValuesHandling handling, [NotNull] params int[] values) {
			#region PreCondition
			IntValuesPreCondition(values);
			#endregion
			NumberListType = GetNumberListTypeFor(handling);
			SetFiledFor(handling, ToSet(values));
		}

		private NumberListType GetNumberListTypeFor(SizeValuesHandling handling) {
			switch (handling) {
				case SizeValuesHandling.Include:
					return NumberListType.Include;
				case SizeValuesHandling.Exclude:
					return NumberListType.Exclude;
				default:
					throw GetInvalidHandling();
			}
		}

		private void SetFiledFor(SizeValuesHandling handling, [NotNull] ISet<int> values) {
			switch (handling) {
				case SizeValuesHandling.Include:
					Include = values;
					break;
				case SizeValuesHandling.Exclude:
					Exclude = values;
					break;
				default:
					throw GetInvalidHandling();
			}
		}

		[NotNull]
		private ArgumentException GetInvalidHandling() {
			return new ArgumentException("Has to be a correct SizeValuesHandling enum value.", "handling");
		}

		[NotNull]
		private ISet<int> ToSet([NotNull] IEnumerable<int> include) {
			return new HashSet<int>(include);
		}

		#region PreCondition
		private void IntValuesPreCondition(int[] values) {
			Debug.Assert(values.Length == values.Distinct().Count(), "the array �values� should be distinct");
		}
		#endregion
	}
}