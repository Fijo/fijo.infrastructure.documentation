using System;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Delegate | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct, Inherited = true, AllowMultiple = true)]
	public class DesignPatternAttribute : Attribute {
		public DesignPatternAttribute(Enums.DesignPattern patternName) {}

		public DesignPatternAttribute(string customPatternName) {}
	}
}