﻿using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[ModificationNote("Some Attributes are partly or fully copied from the ones of ´Enums.DesignPattern.ObserverManager´")]
	[Note("An object that can be watched by an observer")]
	[Note("I use the this more or less as the Management (Service) between the Observerable objects and the Observer - It knows the observer and it have a method Notify that takes an observerable object as argument.")]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternObserver.aspx", LinkNote= "The ´Subject´ is used as this object there, but not the ´ConcreteSubject´. In difference to the website my Subject ist independent from the ´ConcreteSubject´ what is the Observerable for me. It does not inherit from the ´Subject´/ this object. So in some way this is something between the ´Subject´ and the ´Observer´ on the website.")]
	[Related(ObjectType.Class, "Fijo.Infrastructure.Documentation.Attributes.DesignPattern.ObserverAttribute", "a part of the Observer pattern")]
	[Note("more or less a Design Pattern (a part of the Observer pattern)")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	public class ObserverManagerAttribute : DesignPatternAttribute {
		public ObserverManagerAttribute() : base(Enums.DesignPattern.ObserverManager) {}
	}
}