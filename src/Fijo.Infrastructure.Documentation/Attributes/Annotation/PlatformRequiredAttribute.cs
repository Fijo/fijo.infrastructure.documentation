using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	[Desc("Notes, that a class works only on spec. platforms.")]
	public class PlatformRequiredAttribute : Attribute {
		public IList<Platform> Platform { get; private set; }

		public PlatformRequiredAttribute(params Platform[] platform) {
			Platform = platform;
		}
	}
}