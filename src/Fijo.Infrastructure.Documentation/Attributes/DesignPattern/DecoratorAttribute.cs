using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternDecorator.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct | AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
	public class DecoratorAttribute : DesignPatternAttribute {
		public DecoratorAttribute() : base(Enums.DesignPattern.Decorator) {}
	}
}