using System;
using System.ComponentModel;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[Desc("Marks the current abstaction object as an internal abstraction, that should not be used outside and that may be removed, because it is only something to help to for example make structures clearer or reduce redundant code. That attribute exists more or less because of the fact that you cannot make such interfaces/ classes private, protected or internal if they are base classes/ interfaces of public classes/ interfaces.")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false, AllowMultiple = false)]
	[TargetApplyAlthough(typeof (EditorBrowsableAttribute), EditorBrowsableState.Advanced)]
	[Related(ObjectType.Class, "JetBrains.Annotations.PublicAPI", "In some way the oposit of that (it should not be used outside of the project). But it doesn�t have effects like �MeansImplicitUse� or such thinks like PublicAPI have.")]
	public class InternAbstractionAttribute : Attribute {}
}