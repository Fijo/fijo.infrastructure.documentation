using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class PerformanceRisky : Attribute {
		[NotNull, PublicAPI]
		public string Why { get; protected set; }

		public PerformanceRisky([NotNull] string why) {
			Why = why;
		}
	}
}