using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
	public class NotTestableAttribute : Attribute {
		[CanBeNull, PublicAPI]
		public string Reason { get; protected set; }

		public NotTestableAttribute() {}

		public NotTestableAttribute([NotNull] string reason) {
			Reason = reason;
		}
	}
}