using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	public enum NoteType {
		Default = 0,
		Technical
	}
}