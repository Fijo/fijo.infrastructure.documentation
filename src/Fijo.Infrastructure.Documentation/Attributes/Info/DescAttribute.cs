using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[Related(ObjectType.Class, "System.ComponentModel.DescriptionAttribute", "same functionality")]
	[Note("use this instead of DescriptionAttribute")]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class DescAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Message { get; protected set; }

		public DescAttribute([NotNull] string message) {
			Message = message;
		}
	}
}