using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Enums {
	[PublicAPI]
	public enum SizeValuesHandling : byte {
		Include = 1,
		Exclude = 0
	}
}