using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
	[Desc("Descripes an abbreviation that is used in the name of something.")]
	public class AboutNameAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Abbreviation { get; protected set; }
		[NotNull, PublicAPI]
		public string Meaning { get; protected set; }

		public AboutNameAttribute([NotNull] string abbreviation, [NotNull] string meaning) {
			Abbreviation = abbreviation;
			Meaning = meaning;
		}
	}
}