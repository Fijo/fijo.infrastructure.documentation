using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Informs about a supported feture for the attribute target.")]
	public class SupportsAttribute : Attribute {
		[NotNull, PublicAPI]
		public string Feture { get; protected set; }

		public SupportsAttribute([NotNull] string feture) {
			Feture = feture;
		}
	}
}