using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternTemplate.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	public class TemplateMethodAttribute : DesignPatternAttribute {
		public TemplateMethodAttribute() : base(Enums.DesignPattern.TemplateMethod) {}
	}
}