using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Adds a link where you got information about that function or, that your used in the body of this function or something related.")]
	public class LinkAttribute : Attribute {
		[NotNull, PublicAPI, Size(min: 1)]
		public ICollection<string> Links { get; protected set; }

		[Note("Attratches info to be used with the link for example with the words �xyz� are in the link are used for the target object that has the link attribute, about the link or for what it is used/ targeted")]
		[CanBeNull, PublicAPI]
		public string LinkNote { get; set; }

		public LinkAttribute([NotNull, Size(min: 1)] params string[] links) {
			Links = links;
		}
	}
}