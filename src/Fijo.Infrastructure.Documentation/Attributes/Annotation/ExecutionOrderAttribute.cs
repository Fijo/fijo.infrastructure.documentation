using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Constructor | AttributeTargets.Delegate | AttributeTargets.Event | AttributeTargets.Field | AttributeTargets.Module | AttributeTargets.Property | AttributeTargets.Struct, Inherited = true, AllowMultiple = true)]
	[PublicAPI]
	public class ExecutionOrderAttribute : Attribute {
		[PublicAPI] public ExecOrder Order { get; set; }
		[NotNull, PublicAPI] public string SourceObject { get; set; }
		[NotNull, PublicAPI] public string MethodeName { get; set; }

		public ExecutionOrderAttribute(ExecOrder order, [NotNull] string methodeName) : this(order, "this", methodeName) {}

		public ExecutionOrderAttribute(ExecOrder order, [NotNull] string sourceObject, [NotNull] string methodeName) {
			Order = order;
			SourceObject = sourceObject;
			MethodeName = methodeName;
		}
	}
}