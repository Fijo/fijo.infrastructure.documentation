using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternSingleton.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	public class SingletonAttribute : DesignPatternAttribute {
		public SingletonAttribute() : base(Enums.DesignPattern.Singleton) {}
	}
}