using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[Desc("Adds some notes about the target it is applied to")]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class NoteAttribute : Attribute {
		[PublicAPI]
		public NoteType Type { get; protected set; }
		[NotNull, PublicAPI]
		public string Message { get; protected set; }

		public NoteAttribute(NoteType type, [NotNull] string message) {
			Type = type;
			Message = message;
		}

		[Desc("uses NodeType.Default")]
		public NoteAttribute([NotNull] string message) : this(NoteType.Default, message) {}
	}
}