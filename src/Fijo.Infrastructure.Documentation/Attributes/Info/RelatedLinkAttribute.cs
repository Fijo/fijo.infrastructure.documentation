using System;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Desc("Adds a related link to the function, for example of a location where you got some infos how to use something.")]
	public class RelatedLinkAttribute : LinkAttribute {
		public RelatedLinkAttribute([NotNull, Size(min: 1)] params string[] links) : base(links) {}
	}
}