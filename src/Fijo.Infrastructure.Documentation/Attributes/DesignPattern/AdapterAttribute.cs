using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternAdapter.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	public class AdapterAttribute : DesignPatternAttribute {
		public AdapterAttribute() : base(Enums.DesignPattern.Adapter) {}
	}
}