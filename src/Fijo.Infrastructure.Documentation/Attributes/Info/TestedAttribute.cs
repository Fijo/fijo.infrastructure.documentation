using System;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
	public class TestedAttribute : Attribute {
		[CanBeNull, PublicAPI]
		public string TestClassInfo { get; protected set; }

		public TestedAttribute([CanBeNull] string testClassInfo = default(string)) {
			TestClassInfo = testClassInfo;
		}
	}
}