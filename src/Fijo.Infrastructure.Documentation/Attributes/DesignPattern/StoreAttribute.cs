using System;
using Fijo.Infrastructure.Documentation.Attributes.Annotation;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	[UsageInherited(typeof(CannotApplyEqualityOperatorAttribute))]
	public class StoreAttribute : DesignPatternAttribute {
		public StoreAttribute() : base(Enums.DesignPattern.Store) {}
	}
}