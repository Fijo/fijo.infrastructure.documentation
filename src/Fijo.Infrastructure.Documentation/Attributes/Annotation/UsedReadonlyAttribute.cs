using System;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[PublicAPI]
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	[Desc("Notes, that the target is only used as if it where readonly but because of any thing it cannot be readonly so. For exmaple if it have to be set in the constructor of inherited classes (what is not supported by a readonly statement) or something like this.")]
	public class UsedReadonlyAttribute : Attribute {}
}