using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternBuilder.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module , Inherited = true, AllowMultiple = false)]
	public class BuilderAttribute : DesignPatternAttribute {
		public BuilderAttribute() : base(Enums.DesignPattern.Builder) {}
	}
}