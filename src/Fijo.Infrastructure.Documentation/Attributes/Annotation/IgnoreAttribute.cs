using System;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.Annotation {
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Constructor | AttributeTargets.Delegate | AttributeTargets.Event | AttributeTargets.Field | AttributeTargets.Module | AttributeTargets.Property | AttributeTargets.Struct | AttributeTargets.Enum, Inherited = false, AllowMultiple = false)]
	[PublicAPI]
	public class IgnoreAttribute : Attribute {
		[PublicAPI] public IgnoreFor IgnoreFor { get; protected set; }

		public IgnoreAttribute(IgnoreFor ignoreFor) {
			IgnoreFor = ignoreFor;
		}
	}
}