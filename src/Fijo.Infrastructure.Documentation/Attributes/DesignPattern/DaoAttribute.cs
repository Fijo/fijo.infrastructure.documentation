using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[AboutName("D", "Data")]
	[AboutName("A", "Access")]
	[AboutName("O", "Object")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	public class DaoAttribute : DesignPatternAttribute {
		public DaoAttribute() : base(Enums.DesignPattern.Dao) {}
	}
}