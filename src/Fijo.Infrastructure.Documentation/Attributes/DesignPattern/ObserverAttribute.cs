using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Infrastructure.Documentation.Enums;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[PublicAPI]
	[ModificationNote("Some Attributes are partly or fully copied from the ones of ´Enums.DesignPattern.Observer´")]
	[Note("used to watch ObserverSubjects")]
	[Related(ObjectType.Class, "Fijo.Infrastructure.Documentation.Attributes.DesignPattern.ObserverManagerAttribute", "used to watches that object (can be registered to an ´ObserverManager´).")]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternObserver.aspx")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	public class ObserverAttribute : DesignPatternAttribute {
		public ObserverAttribute() : base(Enums.DesignPattern.Observer) {}
	}
}