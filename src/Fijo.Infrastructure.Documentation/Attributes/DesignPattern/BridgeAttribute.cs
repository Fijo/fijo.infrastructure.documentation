using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[PublicAPI]
	[RelatedLink("http://www.dofactory.com/Patterns/PatternBridge.aspx")]
	public class BridgeAttribute : DesignPatternAttribute {
		public BridgeAttribute() : base(Enums.DesignPattern.Bridge) {}
	}
}