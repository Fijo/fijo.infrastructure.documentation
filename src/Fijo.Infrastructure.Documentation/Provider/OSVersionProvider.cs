using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Enums;

namespace Fijo.Infrastructure.Documentation.Provider {
	public static class OSVersionProvider {
		public static readonly IList<WindowsVersion> WindowsVersions = GetWindowsVersions();
		public static readonly WindowsVersion[] IllegalWindowsVersions = new[] {WindowsVersion.DosBased, WindowsVersion.NTBased};

		private static WindowsVersion[] GetWindowsVersions() {
			return Enum.GetValues(typeof (WindowsVersion))
				.Cast<WindowsVersion>()
				.Where(x => !IllegalWindowsVersions.Contains(x))
				.ToArray();
		}
	}
}